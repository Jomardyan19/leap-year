﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp108
{
    class Program
    {
        static void Main(string[] args)
        {
            IsLeapYear(1992);
        }
        public static void IsLeapYear(int year)
        {
            if (year % 4 == 0 && (year % 100 != 0 || year % 400 == 0))
            {
                Console.WriteLine($" {year} is leap year");
            }
            else
            {
                Console.WriteLine($" {year} is not leap year");
            }
        }
    }
}
